import itertools


itertools.permutations([1, 2, 3])


a = [0,1,2,3,4,5,6,7]

for per in itertools.permutations(a):
    cont = True
    for i in range( 0 , len(per) ):
        for j in range( i+1 , len(per) ):
            if( abs(i-j) == abs(per[i]-per[j]) ):
                cont = False
    if(cont==True):
        print(per)
